<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;


class RolesAndPermissionsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Reset cached roles and permissions
        app()[\Spatie\Permission\PermissionRegistrar::class]->forgetCachedPermissions();

        // create permissions
        Permission::updateOrCreate(['name' => 'user.index'], ['guard_name' => 'web']);
        Permission::updateOrCreate(['name' => 'user.create'], ['guard_name' => 'web']);
        Permission::updateOrCreate(['name' => 'user.edit'], ['guard_name' => 'web']);
        Permission::updateOrCreate(['name' => 'user.active'], ['guard_name' => 'web']);
        Permission::updateOrCreate(['name' => 'user.store'], ['guard_name' => 'web']);
        Permission::updateOrCreate(['name' => 'user.show'], ['guard_name' => 'web']);
        Permission::updateOrCreate(['name' => 'user.destroy'], ['guard_name' => 'web']);
        Permission::updateOrCreate(['name' => 'user.update'], ['guard_name' => 'web']);
        Permission::updateOrCreate(['name' => 'user.profile'], ['guard_name' => 'web']);
        Permission::updateOrCreate(['name' => 'permission.index'], ['guard_name' => 'web']);
        Permission::updateOrCreate(['name' => 'settings'], ['guard_name' => 'web']);

          //Create role admin
        $admin = Role::updateOrCreate(['name' => 'Administrador'], ['guard_name' => 'web']);
        $driver = Role::updateOrCreate(['name' => 'Chofer'], ['guard_name' => 'web']);
        $customer = Role::updateOrCreate(['name' => 'Cliente'], ['guard_name' => 'web']);
        
        $admin->givePermissionTo(Permission::all());

        $user = User::find(1);
        if(!$user->hasRole('Administrador')){
            $user->assignRole('Administrador');
        }
    }
}
