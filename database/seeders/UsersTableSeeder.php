<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'name' => 'Administrador',
            'email' => 'admin@seedspa.com',
            'email_verified_at' => now(),
            'password' => Hash::make('secret'),
            'deleted_at' => null,
            'created_at' => now(),
            'updated_at' => now()
        ]);
        // User::updateOrCreate(
        //     ['name' => 'Administrador'],
        //     ['email' => 'admin@seedspa.com'],
        //     ['email_verified_at' => now()],
        //     ['password' => Hash::make('secret')],
        //     ['created_at' => now()],
        //     ['updated_at' => now()]
        // );
    }
}
