@extends('layouts.app', ['activePage' => 'user', 'titlePage' => 'Lista de usuarios'])

@section('content')

<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header card-header-primary">
                        <h4 class="card-title ">Usuarios</h4>
                        <p class="card-category">Aquí puede administrar usuarios</p>
                    </div>
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-6 col-xs-12">
                                @can('user.create')
                                <a href="{{route('user.create')}}" class="btn btn-sm btn-primary">Agregar usuario</a>
                                @endcan
                            </div>
                            <div class="col-md-6 col-xs-12 _float-right">
                                @include('users.search')
                            </div>
                        </div>
                        @if (session('status'))
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="alert alert-success">
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <i class="material-icons">close</i>
                                    </button>
                                    <span>{{ session('status') }}</span>
                                </div>
                            </div>
                        </div>
                        @endif
                        <div class="table-responsive">
                            <table class="table">
                                <thead class=" text-primary">
                                    <tr>
                                        <th>Avatar</th>
                                        <th>Nombre</th>
                                        <th>Correo</th>
                                        <th>Fecha modificación</th>
                                        <th>Rol</th>
                                        <th>Estado</th>
                                        <th class="text-right">Acciones</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($users as $u)
                                    <tr>
                                        <td>
                                            <img class="img-circle elevation-2" style="width:32px"
                                                src="{{$u->getFirstMediaUrl('avatars', 'thumb')}}" alt="default">
                                        </td>
                                        <td>{{$u->name}}</td>
                                        <td>{{$u->email}}</td>
                                        <td>{{$u->updated_at}}</td>
                                        <td>
                                            {{$u->roles[0]->name}}
                                        </td>
                                        @if(empty($u->deleted_at))
                                        <td>
                                            <span class="icon icon-circle s-12  mr-2 text-success">Activo</span>
                                        </td>
                                        @else
                                        <td>
                                            <span class="icon icon-circle s-12  mr-2 text-danger">Inactivo</span>
                                        </td>
                                        @endif
                                        <td class="td-actions text-right">
                                            @if (empty($u->deleted_at))
                                            @can('user.edit')
                                            <a rel="tooltip" class="btn btn-primary btn-link"
                                                href="{{ route('user.edit', $u->id) }}" data-original-title=""
                                                title="Editar">
                                                <i class="material-icons">edit</i>
                                                <div class="ripple-container"></div>
                                            </a>
                                            @endcan
                                            @can('user.destroy')
                                            @if ($u->id !== 1)
                                            <a rel="tooltip" class="btn btn-warning btn-link" href=""
                                                data-target="#modal-delete-{{$u->id}}" data-toggle="modal"
                                                title="Bloquear">
                                                <i class="material-icons">lock</i>
                                                <div class="ripple-container"></div>
                                            </a>
                                            @endif
                                            @endcan
                                            @else
                                            @can('user.active')
                                            <a rel="tooltip" class="btn btn-warning btn-link" href=""
                                                data-target="#modal-delete-{{$u->id}}" data-toggle="modal"
                                                title="Activar">
                                                <i class="material-icons">lock_open</i>
                                                <div class="ripple-container"></div>
                                            </a>
                                            @endcan
                                            @endif
                                        </td>
                                    </tr>
                                    @include('users.modal')
                                    @endforeach
                                </tbody>
                            </table>
                            {!!$users->render()!!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection