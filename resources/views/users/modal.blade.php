<div class="modal fade modal-slide-in-right" id="modal-delete-{{$u->id}}" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				@if(empty($u->deleted_at))
				<h5 class="modal-title">Bloquear usuario</h5>
				@else
				<h5 class="modal-title">Activar usuario</h5>
				@endif
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">×</span>
				</button>
			</div>
			<div class="modal-body">
				<p>{{empty($u->deleted_at) ? 'Al bloquear el usuario no podra acceder al sistema' : 'Al activar el usuario podra acceder al sistema'}}</p>
				<p>Desea continuar ?</p>
			</div>
			<div class="modal-footer">
				{{Form::Open(array('action'=>array('App\Http\Controllers\UserController@destroy',$u->id),'method'=>'delete'))}}
				<button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
				@if(empty($u->deleted_at))
				<button type="submit" class="btn btn-danger">Bloquear</button>
				@else
				<button type="submit" class="btn btn-success">Activar</button>
				@endif
				{{Form::Close()}}
			</div>
		</div>
	</div>
</div>