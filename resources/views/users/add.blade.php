@extends('layouts.app', ['activePage' => 'user', 'titlePage' => 'Agregar usuario'])

@section('css')
<link href="{{asset('material/css/material-pro.css')}}" rel="stylesheet">
@endsection
@section('content')

<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <form method="post" action="{{route('user.store')}}" autocomplete="off" class="form-horizontal " enctype="multipart/form-data">
                    @csrf
                    <div class="card ">
                        <div class="card-header card-header-primary">
                            <p class="card-category">Informacion del usuario</p>
                        </div>
                        <div class="card-body ">
                            @if (session('status'))
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="alert alert-success">
                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                            <i class="material-icons">close</i>
                                        </button>
                                        <span>{{ session('status') }}</span>
                                    </div>
                                </div>
                            </div>
                            @endif
                            <div class="row">
                                <div class="col-md-8 col-xs-12">
                                    <div class="col-xs-12">
                                        <div class="row">
                                            <label class="col-sm-2 col-form-label">Nombre</label>
                                            <div class="col-sm-7">
                                                <div class="form-group{{ $errors->has('name') ? ' has-danger' : '' }}">
                                                    <input class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" name="name" id="input-name" type="text" placeholder="Nombre" value="{{ old('name') }}" required="true" aria-required="true" />
                                                    @if ($errors->has('name'))
                                                    <span id="name-error" class="error text-danger" for="input-name">{{ $errors->first('name') }}</span>
                                                    @endif
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <label class="col-sm-2 col-form-label">Correo</label>
                                            <div class="col-sm-7">
                                                <div class="form-group{{ $errors->has('email') ? ' has-danger' : '' }}">
                                                    <input class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" id="input-email" type="email" placeholder="Correo" value="{{ old('email') }}" required />
                                                    @if ($errors->has('email'))
                                                    <span id="email-error" class="error text-danger" for="input-email">{{ $errors->first('email') }}</span>
                                                    @endif
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">                                            
                                            <label class="col-sm-2 col-form-label">Rol</label>
                                            <div class="col-md-7">
                                                <div class="dropdown bootstrap-select col-sm-12 pl-0 pr-0">
                                                    <select class="selectpicker col-sm-12 pl-0 pr-0" name="role" id="role" data-style="select-with-transition" title="" data-size="100" tabindex="-98" required>
                                                        @foreach($roles as $key => $role)
                                                            <option selected="Cliente" value="{{$role}}">{{$role}}</option>
                                                        @endforeach
                                                    </select>                                                    
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <label class="col-sm-2 col-form-label">Contraseña</label>
                                            <div class="col-sm-7">
                                                <div class="form-group{{ $errors->has('password') ? ' has-danger' : '' }}">
                                                    <input class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" id="input-password" type="password" placeholder="Contraseña" value="" required />
                                                    @if ($errors->has('password'))
                                                    <span id="password-error" class="error text-danger" for="input-password">{{ $errors->first('password') }}</span>
                                                    @endif
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <label class="col-sm-2 col-form-label">Confirmar contraseña</label>
                                            <div class="col-sm-7">
                                                <div class="form-group{{ $errors->has('password_confirmation') ? ' has-danger' : '' }}">
                                                    <input class="form-control{{ $errors->has('password_confirmation') ? ' is-invalid' : '' }}" name="password_confirmation" id="password_confirmation" type="password" placeholder="Confirmar contraseña" value="" required />
                                                    @if ($errors->has('password_confirmation'))
                                                    <span id="password_confirmation-error" class="error text-danger" for="password_confirmation">{{ $errors->first('password_confirmation') }}</span>
                                                    @endif
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4 col-xs-12">
                                    <div class="form-group form-file-upload form-file-multiple">
                                        <input type="file" name="avatar" id="avatar" accept="image/*" class="inputFileHidden file">
                                        <div class="input-group">
                                            <input id="file" type="text" class="form-control inputFileVisible" placeholder="Cargar imagen">
                                            <span class="input-group-btn">
                                                <button type="button" class="browse btn btn-fab btn-round btn-primary">
                                                    <i class="material-icons">attach_file</i>
                                                </button>
                                            </span>
                                        </div>
                                        <img src="{{asset('avatar/default.png')}}" id="preview" class="img-thumbnail">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="card-footer ml-auto mr-auto">
                            <a class="btn btn-danger float-right_" href="{{route('user.index')}}">Volver a la lista</a>
                            <button type="submit" class="btn btn-primary">Guardar</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection
@section('scripts')

@endsection