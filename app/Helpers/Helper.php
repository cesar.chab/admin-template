<?php

use Carbon\Carbon;

use InfyOm\Generator\Common\GeneratorCuisine;
use InfyOm\Generator\Utils\GeneratorCuisinesInputUtil;
use InfyOm\Generator\Utils\HTMLCuisineGenerator;
use Symfony\Component\Debug\Exception\FatalThrowableError;

/**
 * @param $bytes
 * @param int $precision
 * @return string
 */
function formatedSize($bytes, $precision = 1)
{
    $units = array('B', 'KB', 'MB', 'GB', 'TB');

    $bytes = max($bytes, 0);
    $pow = floor(($bytes ? log($bytes) : 0) / log(1024));
    $pow = min($pow, count($units) - 1);

    $bytes /= pow(1024, $pow);

    return round($bytes, $precision) . ' ' . $units[$pow];
}

/**
 * @param $modelObject
 * @param string $attributeName
 * @return null|string|string[]
 */
function formatDateHuman($date)
{
    $replace = Carbon::createFromTimeStamp(strtotime($date))->diffForHumans();
    return $replace;
}

function formatPrice($number, $isBadge = false)
{
    if ($isBadge) {
        return " <spa class='badge badge-primary' style='background-color:#9c27b0'>" .  number_format((float)$number, 2) . "</span>";
    } else {
        return number_format((float)$number, 2);
    }

}

function formatStatus($status)
{
    if (!empty($status)) {
        return " <span class='badge badge-danger' style='background-color:#f44336'>Inactivo</span>";
    } else {
        return " <span class='badge badge-success' style='background-color:#47a44b'>Activo</span>";
    }

}


/**
 * Function to get the name of the month in Spanish
 */
function getMonthName($n)
{
	$months = array(
		'January' => 'Enero',
		'February' => 'Febrero',
		'March' => 'Marzo',
		'April' => 'Abril',
		'May' => 'Mayo',
		'June' => 'Junio',
		'July' => 'Julio',
		'August' => 'Agosto',
		'September' => 'Septiembre',
		'October' => 'Octubre',
		'November' => 'Noviembre',
		'December' => 'Diciembre'
	  );

	$month_num =$n;
	$month_name = date("F", mktime(0, 0, 0, $month_num, 10));

	return $months[$month_name];
}
