<?php

namespace App\Http\Controllers;

use App\Http\Requests\UpdateUserRequest;
use App\Models\User;
use App\Http\Requests\UserRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;
use Spatie\Permission\Models\Role;

class UserController extends Controller
{
    /**
     * Display a listing of the users
     *
     * @param  \App\Models\User  $model
     * @return \Illuminate\View\View
     */
    //public function index(User $model)
    public function index(Request $request)
    {
        $search = trim($request->get('searchText'));
        $users = User::with('roles')->withTrashed()->where('name', 'LIKE', '%' . $search . '%')
            ->orWhere('email', 'LIKE', '%' . $search . '%')
            ->orderBy('id', 'DESC')
            ->paginate(10);
        return view('users.index', ['users' => $users, 'searchText' => $search]);
    }

    public function store(UserRequest $request)
    {
        $input = $request->all();
        $input['password'] = Hash::make($input['password']);
        $user = User::create($input);
        if (isset($input['avatar']) && $input['avatar']) {
            $user->addMediaFromRequest('avatar')->toMediaCollection('avatars');
        }
        $user->assignRole($input['role']);
        Session::flash('status', 'Usuario creado correctamente');
        return redirect('user');
    }

    public function create()
    {
        $roles = Role::all()->pluck('name', 'name');
        return view('users.add', ['roles' => $roles]);
    }

    public function edit($id)
    {
        $user = User::find($id);
        $roles = Role::all()->pluck('name');
        $rolesSelected = $user->roles()->pluck('name')->toArray();
        return view('users.edit', ['user' => $user, 'roles' => $roles, 'rolesSelected' => $rolesSelected]);
    }

    public function update(UpdateUserRequest $request, $id)
    {
        $user = User::find($id);
        // return response()->json($user);
        $input = $request->all();
        if (empty($input['password'])) {
            unset($input['password']);
        } else {
            $input['password'] = Hash::make($input['password']);
        }
        $user->update($input);

        if (isset($input['avatar']) && $request['avatar']) {
            $user->clearMediaCollection('avatars');
            $user->addMediaFromRequest('avatar')->toMediaCollection('avatars');
        }

        //Remove
        //DB::table('model_has_roles')->where('model_id',$id)->delete();
        $user->syncRoles($input['role']);

        if (auth()->user()->id == $id) {
            if (!empty($input['password'])) {
                Auth::logout();
                redirect('/');
            }
        }
        Session::flash('status', 'Usuario actualizado correctamente!');
        return redirect('user');
    }

    public function destroy($id)
    {
        $user = User::withTrashed()->where('id', $id)->first();
        if (empty($user)) {
            Session::flash('status', 'Usuario no encontrado');
            return redirect(route('user.index'));
        }

        if (empty($user->deleted_at)) {
            $user->delete($id);
            Session::flash('status', 'Usuario bloqueado correctamente');
        } else {
            $user->restore();
            Session::flash('status', 'Usuario activado correctamente');
        }

        return redirect(route('user.index'));
    }
}
